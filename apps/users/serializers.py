from .models import MyUser
from rest_framework import serializers


class MyUserSerializer(serializers.ModelSerializer):
    surname = serializers.CharField(source='last_name')
    password = serializers.CharField(write_only=True)

    class Meta:
        fields = ('id', 'first_name', 'surname', 'password', 'phone')
