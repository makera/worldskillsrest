from django.db import models
from django.conf import settings


class Recept(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255, )
    text = models.TextField(verbose_name='Текст', blank=True, )
    photo = models.ImageField(verbose_name='Изображение', upload_to='uploads/', )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Владелец', on_delete=models.CASCADE,
                              related_name='recepts', null=True, )
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name='Пользователи', related_name='shares', )

    def __str__(self):
        return self.name
