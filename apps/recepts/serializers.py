from rest_framework import serializers
from .models import Recept


class ReceptSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    users = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Recept
        fields = ('id', 'name', 'text', 'url', 'photo', 'owner_id', 'users')

    def create(self, validated_data):
        instance = super(ReceptSerializer, self).create(validated_data)
        request = self.context.get('request')
        instance.owner = request.user
        instance.save()
        return instance

    def get_url(self, obj):
        request = self.context.get('request')
        photo_url = obj.photo.url
        return request.build_absolute_uri(photo_url)

    def get_users(self, obj):
        return obj.users.all().values_list('id', flat=True)