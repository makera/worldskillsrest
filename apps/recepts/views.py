from rest_framework import viewsets, permissions, decorators, status
from rest_framework.response import Response
from django.db.models import Q
from .models import Recept
from .serializers import ReceptSerializer


class ReceptViewSet(viewsets.ModelViewSet):
    serializer_class = ReceptSerializer
    permission_classes = [permissions.IsAuthenticated, ]
    http_method_names = ['get', 'post', 'patch', 'delete',]

    def get_queryset(self):
        return Recept.objects.filter(Q(owner=self.request.user) | Q(users=self.request.user))
