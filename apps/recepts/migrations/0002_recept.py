# Generated by Django 2.2 on 2019-12-17 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('recepts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recept',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('text', models.TextField(blank=True, verbose_name='Текст')),
                ('photo', models.ImageField(upload_to='uploads/', verbose_name='Изображение')),
            ],
        ),
    ]
