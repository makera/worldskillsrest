from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import ObjectDoesNotExist
from django.contrib.auth import get_user_model


class TokenAuth(TokenAuthentication):
    keyword = 'Bearer'


class AuthView(APIView):
    def post(self, request):
        errors = {}
        if not 'phone' in request.data:
            errors['phone'] = "Phone is required"
        if not 'password' in request.data:
            errors['password'] = "Password is required"
        if 'phone' in errors or 'password' in errors:
            return Response(errors, status=422)
        try:
            user = get_user_model().objects.get(phone=request.data['phone'])
        except ObjectDoesNotExist:
            return Response({"login": "Incorrect login or password"}, status=404)
        check = user.check_password(request.data['password'])
        if check:
            token = Token.objects.get(user=user)
            return Response({"token": token.key}, status=200)
        else:
            return Response({"errors": "Не верный логин или пароль"}, status=400)


class SignUpView(APIView):
    def post(self, request):
        errors = {}
        if not 'phone' in request.data:
            errors['phone'] = "Phone is required"
        if not 'password' in request.data:
            errors['password'] = "Password is required"
        if not 'first_name' in request.data:
            errors['password'] = "First name is required"
        if not 'surname' in request.data:
            errors['password'] = "Surname is required"
        if 'phone' in errors or 'password' in errors or 'first_name' in errors or 'surname' in errors:
            return Response(errors, status=422)
        try:
            user = get_user_model().objects.create(phone=request.data['phone'],
                                                           first_name=request.data['first_name'],
                                                           last_name=request.data['surname'])
        except ObjectDoesNotExist:
            return Response({"errors": "Не верный логин или пароль"}, status=400)
        user.set_password(request.data['password'])
        return Response({"id": user.id}, status=201)


class LogoutView(APIView):
    def post(self, request):
        return Response(status=200)
