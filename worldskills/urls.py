"""worldskills URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework_nested import routers
from apps.auth_utils import AuthView, SignUpView, LogoutView
from apps.recepts.views import ReceptViewSet

router = routers.SimpleRouter()
router.register('recepts', ReceptViewSet, basename='recepts')

urlpatterns = [
                  path('recepts/admin/', admin.site.urls),
                  path('recepts/api-auth/', include('rest_framework.urls')),
                  path('recepts/api/', include(router.urls)),
                  path('recepts/api/login/', AuthView.as_view()),
                  path('recepts/api/logout/', LogoutView.as_view()),
                  path('recepts/api/signup/', SignUpView.as_view()),
              ] \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
              + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
